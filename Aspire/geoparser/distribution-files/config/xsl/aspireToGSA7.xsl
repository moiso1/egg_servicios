<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/doc">
		<xsl:variable name="action">
			<xsl:value-of select="action" />
		</xsl:variable>
		<xsl:variable name="url">
			<xsl:choose> 
				<xsl:when test="fetchUrl">
					<xsl:choose>
						<xsl:when test="starts-with(fetchUrl, 'file')">
							<xsl:call-template name="string-replace-all">
								<xsl:with-param name="text" select="fetchUrl" />
								<xsl:with-param name="replace" select="'file://'" />
								<xsl:with-param name="by" select="'googleconnector://'" />
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="starts-with(fetchUrl, 'dctm')">
							<xsl:call-template name="string-replace-all">
								<xsl:with-param name="text" select="fetchUrl" />
								<xsl:with-param name="replace" select="'dctm://'" />
								<xsl:with-param name="by" select="'googleconnector://'" />
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="fetchUrl" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="url">
					<xsl:value-of select="url" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="displayUrl" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="makePublic = 'false'">
			<acl>
			    <xsl:attribute name="url">
					<xsl:value-of select="$url"/>
				</xsl:attribute>
				<xsl:for-each select="acls/acl[@scope='global']">
					<principal>
						<xsl:attribute name="scope">
							<xsl:value-of select="@entity" />
						</xsl:attribute>
						<xsl:attribute name="access">
						<xsl:choose>
							<xsl:when test="@access = 'allow'">permit</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="@access" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
						<xsl:value-of select="@fullname" />
					</principal>
				</xsl:for-each>
			</acl>
		</xsl:if>
		<record mimetype="text/plain">
			<xsl:if test="makePublic = 'false'">
				<xsl:attribute name="authmethod">httpbasic</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="url">
				<xsl:value-of select="$url"/>
			</xsl:attribute>
			<xsl:attribute name="action">
				<xsl:choose>
					<xsl:when test="$action = 'add' or $action = 'update'">
						<xsl:text>add</xsl:text>
					</xsl:when>
					<xsl:when test="$action = 'delete'">
						<xsl:text>delete</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:attribute>
			<xsl:if test="$action = 'add' or $action = 'update'">
				<xsl:attribute name="last-modified">
					<xsl:value-of select="lastModified" />
				</xsl:attribute>
				<metadata>
					<!-- GSA metadata -->
					<meta name="google:action">
						<xsl:attribute name="content">
							<xsl:value-of select="$action" />
						</xsl:attribute>
					</meta>
					<meta name="google:ispublic">
						<xsl:attribute name="content">
							<xsl:value-of select="makePublic" />
						</xsl:attribute>
					</meta>
					<meta name="google:mimetype" content="text/plain" />
					<meta name="google:displayurl">
						<xsl:attribute name="content">
							<xsl:choose> 
								<xsl:when test="displayUrl">
									<xsl:value-of select="displayUrl" />
								</xsl:when>
								<xsl:when test="url">
									<xsl:value-of select="url" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="fetchUrl" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
					</meta>
					<xsl:if test="labels">
						<xsl:for-each select="labels/label">
							<meta name="google:label">
								<xsl:attribute name="content">
									<xsl:value-of select="." />
								</xsl:attribute>
							</meta>
						</xsl:for-each>
					</xsl:if>
					<xsl:if test="lastModified and lastModified != ''">
						<meta name="google:lastmodified">
							<xsl:attribute name="content">
								<xsl:value-of select="lastModified" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="title and title != ''">
						<meta name="google:title">
							<xsl:attribute name="content">
								<xsl:value-of select="title" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="author and author != ''">
						<meta name="google:author">
							<xsl:attribute name="content">
								<xsl:value-of select="author" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="comment and comment != ''">
						<meta name="google:comment">
							<xsl:attribute name="content">
								<xsl:value-of select="comment" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="contentStatus and contentStatus != ''">
						<meta name="google:contentstatus">
							<xsl:attribute name="content">
								<xsl:value-of select="contentStatus" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="contentType and contentType != ''">
						<meta name="google:contenttype">
							<xsl:attribute name="content">
								<xsl:value-of select="contentType" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="created and created != ''">
						<meta name="google:created">
							<xsl:attribute name="content">
								<xsl:value-of select="created" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="current and current != ''">
						<meta name="google:current">
							<xsl:attribute name="content">
								<xsl:value-of select="current" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="fileName and fileName != ''">
						<meta name="google:filename">
							<xsl:attribute name="content">
								<xsl:value-of select="fileName" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="fileSize and fileSize != ''">
						<meta name="google:created">
							<xsl:attribute name="content">
								<xsl:value-of select="fileSize" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="homePage and homePage != ''">
						<meta name="google:homepage">
							<xsl:attribute name="content">
								<xsl:value-of select="homePage" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="modifier and modifier != ''">
						<meta name="google:modifier">
							<xsl:attribute name="content">
								<xsl:value-of select="modifier" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="publishDate and publishDate != ''">
						<meta name="google:publishdate">
							<xsl:attribute name="content">
								<xsl:value-of select="publishDate" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="spaceKey and spaceKey != ''">
						<meta name="google:spacekey">
							<xsl:attribute name="content">
								<xsl:value-of select="spaceKey" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="url and url != ''">
						<meta name="google:url">
							<xsl:attribute name="content">
								<xsl:value-of select="url" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="version and version != ''">
						<meta name="google:version">
							<xsl:attribute name="content">
								<xsl:value-of select="version" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<!-- Custom metadata -->

					<xsl:if test="searchFields">
						<xsl:for-each select="searchFields/*">
							<meta>
								<xsl:attribute name="name">
									<xsl:value-of select="name()"/>
								</xsl:attribute>
								<xsl:attribute name="content">
									<xsl:value-of select="." />
								</xsl:attribute>
							</meta>
						</xsl:for-each>
					</xsl:if>

					<!-- Ancestor Metadata -->
					<xsl:if test="hierarchy/item/@id">
						<meta name="itemId">
							<xsl:attribute name="content">
								<xsl:value-of select="hierarchy/item/@id" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="hierarchy/item/@type">
						<meta name="type">
							<xsl:attribute name="content">
								<xsl:value-of select="hierarchy/item/@type" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="hierarchy/item/@name">
						<meta name="name">
							<xsl:attribute name="content">
								<xsl:value-of select="hierarchy/item/@name" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="hierarchy/item/@level">
						<meta name="level">
							<xsl:attribute name="content">
								<xsl:value-of select="hierarchy/item/@level" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="hierarchy/item/ancestors/ancestor[@parent='true']/@id">
						<meta name="parentIds">
							<xsl:attribute name="content">
								<xsl:for-each select="hierarchy/item">    
									<xsl:value-of select="ancestors/ancestor[@parent='true']/@id" />
									<xsl:choose>
										<xsl:when test="position() != last()"><xsl:text> </xsl:text></xsl:when>
									</xsl:choose>
								</xsl:for-each>
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="hierarchy/item/ancestors/ancestor">
						<meta name="ancestorIds">
							<xsl:attribute name="content">
								<xsl:for-each select="hierarchy/item">
									<xsl:for-each select="ancestors/ancestor">
										<xsl:value-of select="@id" />
										<xsl:choose>
											<xsl:when test="position() != last()"><xsl:text> </xsl:text></xsl:when>
										</xsl:choose>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="hierarchy/item/ancestors/ancestor">
						<meta name="ancestorsDisplay">
							<xsl:attribute name="content">
								<xsl:for-each select="hierarchy/item">
									<!-- {hierarchy} -->
									<xsl:for-each select="ancestors/ancestor">
										<xsl:sort select="@level" order="ascending" />
										<!-- {ancestor} -->
										<xsl:value-of select="@type" />
										<xsl:text>|</xsl:text>
										<xsl:value-of select="@id" />
										<xsl:text>|</xsl:text>
										<xsl:value-of select="@name" />
										<!-- {ancestor} -->
										<xsl:choose>
											<xsl:when test="position() != last()">\\</xsl:when>
										</xsl:choose>
									</xsl:for-each>
									<!-- {hierarchy} -->
									<xsl:choose>
										<xsl:when test="position() != last()">\\\</xsl:when>
									</xsl:choose>
								</xsl:for-each>
							</xsl:attribute>
						</meta>
					</xsl:if>

					<xsl:if test="sourceName and sourceName != ''">
						<meta name="sourceName">
							<xsl:attribute name="content">
								<xsl:value-of select="sourceName" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="sourceType and sourceType != ''">
						<meta name="sourceType">
							<xsl:attribute name="content">
								<xsl:value-of select="sourceType" />
							</xsl:attribute>
						</meta>
					</xsl:if>
					<xsl:if test="slicingToken and slicingToken != ''">
						<meta name="slicingToken">
							<xsl:attribute name="content">
								<xsl:value-of select="slicingToken" />
							</xsl:attribute>
						</meta>
					</xsl:if>
				</metadata>
				<xsl:if test="content">
					<content>
						<xsl:value-of select="'&lt;![CDATA['"
							disable-output-escaping="yes" />
						<xsl:value-of select="content" />
						<xsl:value-of select="']]&gt;'"
							disable-output-escaping="yes" />
					</content>
				</xsl:if>
			</xsl:if>
		</record>
	</xsl:template>
	<xsl:template name="string-replace-all">
		<xsl:param name="text" />
		<xsl:param name="replace" />
		<xsl:param name="by" />
		<xsl:choose>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text,$replace)" />
				<xsl:value-of select="$by" />
				<xsl:call-template name="string-replace-all">
					<xsl:with-param name="text"
						select="substring-after($text,$replace)" />
					<xsl:with-param name="replace" select="$replace" />
					<xsl:with-param name="by" select="$by" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>