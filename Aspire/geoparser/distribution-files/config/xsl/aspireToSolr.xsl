<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/doc">
					<doc>
						<xsl:apply-templates />

					</doc>
	</xsl:template>
	<xsl:template match="geonameid | name | asciiname | alternatenames | latitude | longitude | feature_class | feature_code| country_code | population |  elevation">
		<field>
			<xsl:attribute name="name">
				<xsl:value-of select="name(.)" />
			</xsl:attribute>
			<xsl:value-of select="." />
		</field>
			
	</xsl:template>
	
	<xsl:template match="*"/>
</xsl:stylesheet>