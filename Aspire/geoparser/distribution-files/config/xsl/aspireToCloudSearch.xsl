<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/doc">
    <xsl:variable name="action"><xsl:value-of select="action"/></xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$action = 'delete'">
        <delete>
          <xsl:attribute name="version"><xsl:value-of select="version" /></xsl:attribute>
          <xsl:attribute name="id"><xsl:value-of select="cloudSearchId" /></xsl:attribute>
        </delete>
      </xsl:when>
      
      <xsl:otherwise>
        <add lang="en">
          <xsl:attribute name="version"><xsl:value-of select="version" /></xsl:attribute>
          <xsl:attribute name="id"><xsl:value-of select="cloudSearchId" /></xsl:attribute>

          <xsl:if test="title">
            <field name="title"><xsl:value-of select="title" /></field>
            <field name="title_search">
              <xsl:text>XXPREFIXXX </xsl:text><xsl:value-of select="title" /><xsl:text> XXSUFFIXXX</xsl:text>
            </field>
          </xsl:if>

          <xsl:if test="lastModified">
            <field name="last_modified"><xsl:value-of select="lastModifiedMins"/></field>
          </xsl:if>

          <xsl:if test="content">
            <field name="content"><xsl:value-of select="content" /></field>
          </xsl:if>

          <xsl:if test="dataSize">
            <field name="content_size"><xsl:value-of select="dataSize" /></field>
          </xsl:if>

          <xsl:if test="teaser">
            <field name="teaser"><xsl:value-of select="teaser" /></field>
          </xsl:if>

          <field name="url">
            <xsl:choose> 
              <xsl:when test="fetchUrl"><xsl:value-of select="fetchUrl"/></xsl:when>
              <xsl:when test="url"><xsl:value-of select="url"/></xsl:when>
              <xsl:when test="displayUrl"><xsl:value-of select="displayUrl"/></xsl:when>
              <xsl:otherwise>URL-NOT-PROVIDED</xsl:otherwise>
            </xsl:choose>
          </field>

          <xsl:for-each select="acls/acl[(@type='allow' or @access='allow') and @scope='global']">
            <field name="allow_acls"><xsl:value-of select="@fullName"/></field>
          </xsl:for-each>
          <xsl:for-each select="acls/acl[(@type='deny' or @access='deny') and @scope='global']">
            <field name="deny_acls"><xsl:value-of select="@name"/></field>
          </xsl:for-each>

          <!-- Ancestor Metadata -->
          <xsl:if test="hierarchy/itemId/@md5">
            <field name="item_id"><xsl:value-of select="hierarchy/itemId/@md5"/></field>
          </xsl:if>
          <xsl:if test="hierarchy/itemType">
            <field name="item_type"><xsl:value-of select="hierarchy/itemType"/></field>
            <field name="f_item_type"><xsl:value-of select="hierarchy/itemType"/></field>
          </xsl:if>
          <xsl:if test="hierarchy/itemName">
            <field name="item_name"><xsl:value-of select="hierarchy/itemName"/></field>
          </xsl:if>
          <xsl:if test="hierarchy/itemLevel">
            <field name="item_level"><xsl:value-of select="hierarchy/itemLevel"/></field>
          </xsl:if>

          <xsl:if test="hierarchy/parentId/@md5">
            <field name="parent_id"><xsl:value-of select="hierarchy/parentId/@md5"/></field>
          </xsl:if>

          <xsl:if test="hierarchy/ancestorTypes">
            <field name="ancestor_types"><xsl:value-of select="hierarchy/ancestorTypes"/></field>
          </xsl:if>
          <xsl:if test="hierarchy/ancestorsDisplay">
            <field name="ancestors_display"><xsl:value-of select="hierarchy/ancestorsDisplay"/></field>
          </xsl:if>
          <xsl:if test="hierarchy/ancestors">
            <xsl:for-each select="hierarchy/ancestors/ancestorId">
              <field name="ancestors_id"><xsl:value-of select="@md5"/></field>
            </xsl:for-each>
          </xsl:if>

          <!-- Connector Display Name -->
          <xsl:if test="sourceName">
            <field name="source_name"><xsl:value-of select="sourceName"/></field>
            <field name="f_source_name"><xsl:value-of select="sourceName"/></field>
          </xsl:if>
          <xsl:if test="sourceType">
            <field name="source_type"><xsl:value-of select="sourceType"/></field>
            <field name="f_source_type"><xsl:value-of select="sourceType"/></field>
          </xsl:if>

        </add>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>