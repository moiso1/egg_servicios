<?xml version="1.0" encoding="UTF-8"?>
<!--
 ****************************************************************************
 * (c) Search Technologies 2011
 *
 * Simple style sheet for transforming a flat Aspire document to FASTXml
 *
 * Author: Steve Denny
 *
 ****************************************************************************
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <document>
      <xsl:for-each select="doc/child::*">
        <element>
          <xsl:attribute name="name">
          	<xsl:value-of select="name()"/>
          </xsl:attribute>
		  <value>
		    <xsl:value-of select="."/>
          </value>		
		</element>
      </xsl:for-each>
    </document>
  </xsl:template>
</xsl:stylesheet>