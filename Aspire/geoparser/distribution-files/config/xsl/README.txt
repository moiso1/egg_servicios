This directory contains *sample* XSL transforms for various search engine platforms.
It is the recommended directory for XSL transforms.

Note that these transforms are NOT used by the standard "Post To XXX" application 
bundles produced by Search Technologies by default. Those bundles will contain their 
own XSL transforms. Most of the Post to XXX application bundles do allow the administrator
to change this default behavior to customize your XSL transformations.

