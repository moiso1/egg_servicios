// Function that process the children of a connector specific field
def getChildren(name, parent) {

  builder."$name"() {
    parent.getChildren().each() { val ->
          
      def attr = val.getName();
      
      //if it has other children
      if(val.getChildren().size() > 0) {
        getChildren(attr, val);
      }else {
        
        builder."$attr"() {
          
          //All the attributes
          val.getAttributeNames().each() { attrName ->
            "@$attrName" val.getAttribute(attrName);
          }
          
          //Main content
          '_$' val.getText();
        }
      }
    }
  }
}


//Action of the job
String action = doc.action.getText();

/*****************
 * Add or Update *
 *************************************************/
  
if ((action == "add") || (action == "update")){
  
  //ElascticSearch Header
  builder.index() {                     
    //Get Type
    if(doc.repItemType != null) {
      '_type' doc.repItemType.getText()
    }else {
      '_type' "TYPE-NOT-PROVIDED"
    }
    
    '_index' doc.elasticIndex
    
    //Get ID
    if (doc.fetchUrl != null) {
      '_id' doc.fetchUrl
    }else if (doc.url != null){
      '_id' doc.url
    }else if (doc.displayUrl != null){
      '_id' doc.displayUrl
    }else if (doc.id != null){
      '_id' doc.id
    }else {
      '_id' "ID-NOT-PROVIDED"
    } 
  }
  
  builder.flush()
  
  //Document Source
  builder.$object() {
    
    if(doc.name != null) {
      name doc.name
    }
    
    if(doc.owner != null) {
      author doc.owner
    }
    
    if(doc.lastModified != null) {
      last_modified doc.lastModified;
    }
    
    if(doc.title != null) {
      title doc.title
    }
    
    if (doc.fetchUrl != null) {
      url doc.fetchUrl
    }else if (doc.url != null){
      url doc.url
    }else if (doc.displayUrl != null){
      url doc.displayUrl
    }else {
      url "URL-NOT-PROVIDED"
    } 
    
    if (doc.displayUrl != null) {
      displayurl doc.displayUrl
    }else if (doc.url != null){
      displayurl doc.url
    }else if (doc.fetchUrl != null){
      displayurl doc.fetchUrl
    }
    
    if(doc.dataSize != null) {
      'size' doc.dataSize
    }
    
    if(doc.sourceName != null) {
     sourceName doc.sourceName
    }
    
    if(doc.sourceType != null) {
      sourceType doc.sourceType
    }
    
    //Connector Specific
    if (doc.connectorSpecific != null) { 
      builder.connectorSpecific() {
        doc.connectorSpecific.getChildren().each() { val ->
          
          def attr = val.getAttribute("name");
        
          if(val.getChildren().size() > 0) {
            getChildren(attr, val);
          }else if (val.getText() != null){
            "$attr" val.getText();
          }
        }
      }
    }
    
    //Search Fields
    if (doc.searchFields != null) { 
      doc.searchFields.getChildren().each() { val ->
        
        def attr = val.getName();
      
        "$attr" val.getText();
      }
    }
    
    //ACLs
  if (doc.acls != null) {
  
   boolean groups = false;
   boolean users = false;
  
   doc.acls.getChildren().each() { val ->
   
    if (val.getAttribute("entity") == "group") {
     groups = true;
    } else if (val.getAttribute("entity") == "user") {
     users = true;
    }
   }
   
   builder.acls() {

    'type' "nested";
        
    if(groups) {
    
     builder.groups {
     
      'type' "nested";
     
      doc.acls.getChildren().each() { val ->
        
       def name = val.getAttribute("name").replaceAll("[:| ]", "_")
        
       if (val.getAttribute("entity") == "group")
        "$name" val.getAttribute("access")
      }
     }
    }
    
    if(users) {
     builder.users {
     
      'type' "nested";
     
      doc.acls.getChildren().each() { val ->
        
       def name = val.getAttribute("name").replaceAll("[:| ]", "_")
        
       if (val.getAttribute("entity") == "user")
        "$name" val.getAttribute("access")
      }
     }
    }
    
    if (!(users || groups)) {
     builder.groups {
     
      'type' "nested";
        
      def name = "PUBLIC_ALL"
       
      "$name" "allow"
     }
    }
   }
   
  }
    
    if(doc.content != null) {
      content doc.content.getText()
    }
    
    submitTime (new Date());
  }
    
  
}else {
  
/**********
 * Delete *
 *************************************************/
  
  //ElascticSearch Header
  builder.delete() {
    //Get Type
    if(doc.repItemType != null) {
      '_type' doc.repItemType.getText()
    }else {
      '_type' "TYPE-NOT-PROVIDED"
    }
    
    '_index' doc.elasticIndex
    
    //Get ID
    if (doc.fetchUrl != null) {
      '_id' doc.fetchUrl
    }else if (doc.url != null){
      '_id' doc.url
    }else if (doc.displayUrl != null){
      '_id' doc.displayUrl
    }else if (doc.id != null){
      '_id' doc.id
    }else {
      '_id' "ID-NOT-PROVIDED"
    } 
  }
}