#!/bin/sh
#
# Wrapper script to start FELIX
#
# USAGE: encryptPassword.sh
#

# Set the java options
JAVA_OPT="$JAVA_OPT -Xmx1024m"

if [ `uname -s` = "Darwin" ]
then
  # Note: readlink -f is not available on MacOS X
  SCRIPT="$0"
  while [ -h "$SCRIPT" ] ; do
    ls=`ls -ld "$SCRIPT"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
      SCRIPT="$link"
    else
      SCRIPT=`dirname "$SCRIPT"`/"$link"
    fi
  done
else
  SCRIPT=`readlink -f $0`
fi

# get the directory of the script
SCRIPT_DIR=`dirname $SCRIPT`
SCRIPT_PARENT_DIR=`dirname $SCRIPT_DIR`

# default the aspire home if it's not set.
# the default will assume this script is in the bin directory under
# $ASPIRE_HOME and work back from there.

if [ "${ASPIRE_HOME}" = "" ]
then
  # Set ASPIRE_HOME to the next directory up
  export ASPIRE_HOME=$SCRIPT_PARENT_DIR
fi

# Startup
cd ${ASPIRE_HOME}
rm -rf cache >/dev/null 2>&1
bin/aspiresh.sh bin/encryptionApp.xml